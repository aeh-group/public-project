This is a development repo for experimenting with homebrew formulas that, when ready for production, be migrated to https://gitlab.oit.duke.edu/aeh-group/homebrew-chem  assuming the formulas developed here do not (yet) exist in other taps. We may find no tap that accepts a pull request. Or original authors are unreachable, and/or do not want to support a homebrew tap. In this latter case, we may need to put such formula in a duke-only homebrew tap.

#### Random notes
* mfold_util is a subset of mfold