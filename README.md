### Aeh-group Duke

Hargrove-group's public homebrew tap and project space visible to the world (public="he project can be accessed without any authentication."

See the wiki for "howto" quick-start guides.

#### How do I install these formulae?
`brew install aeh-group/duke/<formula>`

Or `brew tap aeh-group/duke` and then `brew install <formula>`.

#### Documentation

Pre-requisites: you must first install `brew` on macos, linux, or inside WSL/WSL2 on windows per: https://docs.brew.sh/Installation

Once brew is installed:
`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).

